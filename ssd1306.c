#include "ssd1306.h"
#include "twi.h" //SSD1306 require I2C aka TWI interface thats why I include it here.

void ssd1306_init(void)
{
  twi_init();
  ssd1306_write_command(0xAE); //display off
  ssd1306_write_command(0x00); //Set Memory Addressing Mode
  ssd1306_write_command(0x10); //00,Horizontal Addressing Mode;01,Vertical Addressing Mode;10,Page Addressing Mode (RESET);11,Invalid
  ssd1306_write_command(0x40); //Set Page Start Address for Page Addressing Mode,0-7
  ssd1306_write_command(0x81); //Set COM Output Scan Direction
  ssd1306_write_command(0xCF); //---set low column address
  ssd1306_write_command(0xA1); //---set high column address
  ssd1306_write_command(0xC8); //--set start line address
  ssd1306_write_command(0xA6); //--set contrast control register
  ssd1306_write_command(0xA8);
  ssd1306_write_command(0x3F); //--set segment re-map 0 to 127
  ssd1306_write_command(0xD3); //--set normal display
  ssd1306_write_command(0x00); //--set multiplex ratio(1 to 64)
  ssd1306_write_command(0xD5); //
  ssd1306_write_command(0x80); //0xa4,Output follows RAM content;0xa5,Output ignores RAM content
  ssd1306_write_command(0xD9); //-set display offset
  ssd1306_write_command(0xF1); //-not offset
  ssd1306_write_command(0xDA); //--set display clock divide ratio/oscillator frequency
  ssd1306_write_command(0x12); //--set divide ratio
  ssd1306_write_command(0xDB); //--set pre-charge period
  ssd1306_write_command(0x40); //
  ssd1306_write_command(0x20); //--set com pins hardware configuration
  ssd1306_write_command(0x02);
  ssd1306_write_command(0x8D); //--set vcomh
  ssd1306_write_command(0x14); //0x20,0.77xVcc
  ssd1306_write_command(0xA4); //--set DC-DC enable
  ssd1306_write_command(0xA6); //
  ssd1306_write_command(0xAF); //--turn on oled panel 

#ifdef SSD1306_DEMO
  ssd1306_demo();
#endif
}

void ssd1306_write_command(unsigned char command)
{
  twi_start();
  twi_write(0x78); //Slave address,SA0=0
  twi_write(0x00); //write command
  twi_write(command);
  twi_stop();
}

void ssd1306_begin_data(void)
{
  twi_start();
  twi_write(0x78);
  twi_write(0x40); //write data
}

void ssd1306_set_pos(unsigned char x, unsigned char y)
{
  twi_start();
  twi_write(0x78); //Slave address,SA0=0
  twi_write(0x00); //write command
  twi_write(0xb0+y);
  twi_write(((x&0xf0)>>4)|0x10); //|0x10
  twi_write((x&0x0f)|0x01);      //|0x01
  twi_stop();
}

void ssd1306_fill_screen(unsigned char fill_data)
{
  unsigned char m,n;
  for(m=0;m<8;m++)
  {
    ssd1306_write_command(0xb0+m); //page0-page1
    ssd1306_write_command(0x00);   //low column start address
    ssd1306_write_command(0x10);   //high column start address
    ssd1306_begin_data();
    for(n=0;n<128;n++)
    {
      twi_write(fill_data);
    }
    twi_stop();
  }
}

#ifdef FONT_TYPE_6x8
void ssd1306_send_6x8_char(unsigned char x, unsigned char y, const char ch[])
{
  unsigned char c,i,j=0;
  while(ch[j] != '\0')
  {
    c = ch[j] - 32;
    if(x>126)
    {
      x=0;
      y++;
    }
    ssd1306_set_pos(x,y);
    ssd1306_begin_data();
    for(i=0;i<6;i++)
    {
      twi_write(pgm_read_byte(&font6x8[c*6+i]));
    }
    twi_stop();
    x += 6;
    j++;
  }
}
#endif

#ifdef FONT_TYPE_8x16
void ssd1306_send_8x16_char(unsigned char x, unsigned char y,const char ch[])
{
  unsigned char c=0,i=0,j=0;
  while (ch[j]!='\0')
  {
    c =ch[j]-32;
    if(x>120)
    {
      x=0;
      y++;
    }
    ssd1306_set_pos(x,y);
    ssd1306_begin_data();
    for(i=0;i<8;i++)
    {
      twi_write(pgm_read_byte(&font8x16[c*16+i]));
    }
    twi_stop();
    ssd1306_set_pos(x,y+1);
    ssd1306_begin_data();
    for(i=0;i<8;i++)
    {
      twi_write(pgm_read_byte(&font8x16[c*16+i+8]));
    }
    twi_stop();
    x+=8;
    j++;
  }
}
#endif

#ifdef FONT_TYPE_16x16
/* This method i keep just for example, its display chinees number. */
void ssd1306_send_16x16_uint(unsigned char x, unsigned char y, unsigned char N)
{
  unsigned char wm=0;
  unsigned int adder=32*N;
  ssd1306_set_pos(x , y);
  ssd1306_begin_data();
  for(wm = 0;wm < 16;wm++)
  {
    twi_write(pgm_read_byte(&font16x16[adder]));
    adder += 1;
  }
  twi_stop();
  ssd1306_set_pos(x,y + 1);
  ssd1306_begin_data();
  for(wm = 0;wm < 16;wm++)
  {
    twi_write(pgm_read_byte(&font16x16[adder]));
    adder += 1;
  }
  twi_stop();
}
#endif

void ssd1306_draw_bmp(unsigned char x0, unsigned char y0, unsigned char x1, unsigned char y1,const char BMP[])
{
  unsigned int j=0;
  unsigned char x,y;
  
  if(y1%8==0)
    y=y1/8;
  else
    y=y1/8+1;
  for(y=y0;y<y1;y++)
  {
    ssd1306_set_pos(x0,y);
    ssd1306_begin_data();
    for(x=x0;x<x1;x++)
    {
      twi_write(pgm_read_byte(&BMP[j++]));
    }
    twi_stop();
  }
}

#ifdef SSD1306_DEMO
#include <util/delay.h>   //delay
/* This is just demo function to see if display works fine. */
void ssd1306_demo(void){
  _delay_ms(10);
  ssd1306_fill_screen(0x00);

  ssd1306_send_char(0,0,"Grzegorz Hetman");
  ssd1306_send_char(0,2,"SSD1306 Demo");
  ssd1306_send_char(0,4,"hetii@tlen.pl");
  ssd1306_send_char(0,6,"+48 606391258");

  _delay_ms(3000);
  ssd1306_fill_screen(0xf0);
  _delay_ms(2000);
  ssd1306_fill_screen(0xff);
  _delay_ms(2000);
  ssd1306_draw_bmp(0,0,128,8, DEMO_LOGO);
}
#endif
