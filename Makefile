# Name: Makefile
# Project: SSD1306 C driver
# Author: Grzegorz Hetman
# Creation Date: 2014-08-07
# Tabsize: 4

DEVICE  = atmega168
#DEVICE  = atmega328p
F_CPU   = 12000000
FUSE_L  = 0xEF
FUSE_H  = 0xC9

# AVRDUDE = avrdude -c usbasp -p $(DEVICE) # edit this line for your programmer
AVRDUDE = sudo avrdude -V -c stk500v2 -P avrdoper -p $(DEVICE) -F
#AVRDUDE = avrdude -c usbasp -p $(DEVICE) -B 200 

CFLAGS  = -I.

# list all 'c' files.
SRCS	= $(shell find . -name '*.c' && find . -name '*.asm')

# define the C object files 
# This uses Suffix Replacement within a macro:
#   $(name:string1=string2)
#         For each word in 'name' replace 'string1' with 'string2'
# Below we are replacing the suffix .c of all words in the macro SRCS
# with the .o suffix

O = $(SRCS:.c=.o)
OBJECTS = $(O:.asm=.o)

COMPILE = avr-gcc -Wall -Os -DF_CPU=$(F_CPU) -DF_OSC=$(F_CPU) $(CFLAGS) -mmcu=$(DEVICE) 

# symbolic targets:
help:
	@echo "This Makefile has no default rule. Use one of the following:"
	@echo "make hex ....... to build main.hex"
	@echo "make program ... to flash fuses and firmware"
	@echo "make fuse ...... to flash the fuses"
	@echo "make flash ..... to flash the firmware (use this on metaboard)"
	@echo "make clean ..... to delete objects and hex file"
	@echo "make rs ........ launch rs232 serial console"

hex: main.hex

program: flash fuse
reflash: clean flash

# rule for programming fuse bits:
fuse:
	@[ "$(FUSE_H)" != "" -a "$(FUSE_L)" != "" ] || \
		{ echo "*** Edit Makefile and choose values for FUSE_L and FUSE_H!"; exit 1; }
	$(AVRDUDE) -U hfuse:w:$(FUSE_H):m -U lfuse:w:$(FUSE_L):m

# rule for uploading firmware:
flash: main.hex
	$(AVRDUDE) -U flash:w:main.hex:i

# rule for deleting dependent files (those which can be built by Make):
clean:
	rm -f main.hex main.lst main.obj main.cof main.list main.map main.eep.hex main.elf *.o usbdrv/*.o main.s usbdrv/oddebug.s usbdrv/usbdrv.s atboard/*.o

# Generic rule for compiling C files:
.c.o:
	$(COMPILE) -c $< -o $@

# Generic rule for assembling Assembler source files:
.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@
# "-x assembler-with-cpp" should not be necessary since this is the default
# file type for the .S (with capital S) extension. However, upper case
# characters are not always preserved on Windows. To ensure WinAVR
# compatibility define the file type manually.

# Generic rule for compiling C to assembler, used for debugging only.
.c.s:
	$(COMPILE) -S $< -o $@

# file targets:

# Optionally get latest usbdrv.
usbdrv:
	git clone https://github.com/obdev/v-usb && mv v-usb/usbdrv . && rm -rf v-usb

main.elf: $(OBJECTS)	# usbdrv dependency only needed because we copy it
	$(COMPILE) -o main.elf $(OBJECTS)

main.hex: main.elf
	rm -f main.hex main.eep.hex
	avr-objcopy -j .text -j .data -O ihex main.elf main.hex
	avr-size main.hex

# debugging targets:

disasm:	main.elf
	avr-objdump -d main.elf

cpp:
	$(COMPILE) -E main.c

rs:
	echo "import serial, time\n\
	s = serial.Serial(port='/dev/ttyUSB0', baudrate=9600)\n\
	i=0\n\
	while s.isOpen():\n\
	    time.sleep(0.05)\n\
	    if s.inWaiting():\n\
	        print i,\"\\\t\",s.readline().strip()\n\
	        i+=1" | python -

