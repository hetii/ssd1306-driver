#include <avr/io.h>
#include "macros.h"
#include "twi.h"

void twi_init(void){
#ifdef USE_HARDWARE
  TWBR = (F_CPU / 100000UL - 16) / 2;
#else
  SET_OUTPUT(HWPIN_SDA);
  SET_OUTPUT(HWPIN_SCL); 
#endif
}

void twi_start(void)
{
#ifdef USE_HARDWARE
  TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
  while (!(TWCR & (1<<TWINT)));
#else
  SET(HWPIN_SCL);
  SET(HWPIN_SDA);
  RESET(HWPIN_SDA);
  RESET(HWPIN_SCL);
#endif
}

void twi_stop(void)
{
#ifdef USE_HARDWARE
  TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
  while ((TWCR & (1<<TWSTO)));
#else
  RESET(HWPIN_SCL);
  RESET(HWPIN_SDA);
  SET(HWPIN_SCL);
  SET(HWPIN_SDA);
#endif
}

void twi_write(char data)
{
#ifdef USE_HARDWARE
  TWDR = data;
  TWCR = (1<<TWINT) | (1<<TWEN);
  while (!(TWCR & (1<<TWINT)));
#else
  unsigned char i;
  for(i=0;i<8;i++)
  {
    if((IIC_Byte << i) & 0x80)
    {
      SET(HWPIN_SDA);
    } else {
      RESET(HWPIN_SDA); 
    }
      SET(HWPIN_SCL);
      RESET(HWPIN_SCL);   
      //IIC_Byte<<=1;
  }
  SET(HWPIN_SDA);
  SET(HWPIN_SCL);
  RESET(HWPIN_SCL);
#endif
}

char twi_read(char ack)
{
#ifdef USE_HARDWARE
  TWCR = ack
  ? ((1 << TWINT) | (1 << TWEN) | (1 << TWEA))
  : ((1 << TWINT) | (1 << TWEN)) ;
  while (!(TWCR & (1<<TWINT)));
  return TWDR;
#else

#endif
}
